package ru.mrbrikster.chatty.chat;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.mrbrikster.baseplugin.config.Configuration;
import ru.mrbrikster.chatty.Chatty;

import java.io.*;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;

public class JsonStorage {

    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    private final File storageFile;
    private final Configuration configuration;

    public JsonStorage(Chatty chatty) {
        this.configuration = chatty.getExact(Configuration.class);
        this.storageFile = new File(chatty.getDataFolder(), "storage.json");

        if (!storageFile.exists()) {
            try {
                if (!storageFile.createNewFile()) {
                    throw new IOException("Cannot create storage.json");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void setProperty(String player, String property, JsonElement value) {
        JsonElement jsonObject;
        jsonObject = read();
        if (jsonObject == null || !jsonObject.isJsonObject())
            jsonObject = new JsonObject();
        else jsonObject = jsonObject.getAsJsonObject();

        JsonElement propertyElement;
        if (((JsonObject) jsonObject).has(property)) {
            propertyElement = ((JsonObject) jsonObject).remove(property);
            if (propertyElement.isJsonObject())
                propertyElement = propertyElement.getAsJsonObject();
            else propertyElement = new JsonObject();
        } else propertyElement = new JsonObject();

        if (((JsonObject) propertyElement).has(player))
            ((JsonObject) propertyElement).remove(player);

        if (value != null)
            ((JsonObject) propertyElement).add(player, value);

        ((JsonObject) jsonObject).add(property, propertyElement);

        write(GSON.toJson(jsonObject));
    }

    public void setProperty(Player player, String property, JsonElement value) {
        if (configuration.getNode("general.uuid").getAsBoolean(false)) {
            setProperty(player.getUniqueId().toString(), property, value);
        } else {
            setProperty(player.getName(), property, value);
        }
    }

    private Optional<JsonElement> getProperty(String player, String property) {
        JsonElement jsonObject = read();
        if (jsonObject == null || !jsonObject.isJsonObject())
          return Optional.empty();
        JsonElement propertyElement = ((JsonObject) jsonObject).get(property);
        if (propertyElement == null)
            return Optional.empty();
        if (propertyElement.isJsonObject()) {
            JsonElement playerPropertyElement = propertyElement.getAsJsonObject().get(player);
            return Optional.ofNullable(playerPropertyElement);
        }
        return Optional.empty();
    }

    public Optional<JsonElement> getProperty(Player player, String property) {
        if (configuration.getNode("general.uuid").getAsBoolean(false)) {
            return getProperty(player.getUniqueId().toString(), property);
        } else {
            return getProperty(player.getName(), property);
        }
    }

    public boolean isIgnore(CommandSender recipient, CommandSender sender) {
        if (recipient instanceof Player) {
            JsonElement jsonElement = getProperty((Player) recipient, "ignore").orElseGet(JsonArray::new);

            if (!jsonElement.isJsonArray())
                jsonElement = new JsonArray();
            Type type = new TypeToken<List<String>>(){}.getType();
            List<String> ignoringPlayers = GSON.fromJson(jsonElement, type);
            return ignoringPlayers.stream().anyMatch(player-> player.equalsIgnoreCase(sender.getName()));
        }

        return false;
    }

    private JsonElement read(){
        try {
            return GSON.fromJson(new FileReader(storageFile), JsonElement.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void write(String json) {
        BufferedWriter writer;
        try {
            writer = new BufferedWriter(new FileWriter(storageFile));
            writer.write(json);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
